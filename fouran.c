/**************************************************************************
#   Copyright (C) 2016 by Ivan Fedyanin                                   *
#   octy@xrlab.ineos.ac.ru                                                *
#                                                                         *
#   This file is a part of fouran program distribution                    *
#                                                                         *
#   fouran is free software: you can redistribute it and/or modify        *
#   it under the terms of the GNU General Public License as published by  *
#   the Free Software Foundation, either version 3 of the License, or     *
#   (at your option) any later version.                                   *
#                                                                         *
#   This program is distributed in the hope that it will be useful,       *
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#   GNU General Public License for more details.                          *
#                                                                         *
#   You should have received a copy of the GNU General Public License     *
#   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
#                                                                         *
#*************************************************************************/
#include <omp.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <strings.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>
#include <errno.h>

#define P_NAME "fouran"
#define MAX_LINE_LENGTH 120
#define DATA_W 18
#define DEFAULT_BIN_WIDTH 0.01

int is_debug = 0;

void print_help()
{
    // FIXME
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "%s [-Dh] [-f fou_name] [-m mas_name] [-d dat_name]\n[-o out_name] [-w bin_width]\n\n", P_NAME);
    fprintf(stderr, "GNU-style options:\n\n");
    fprintf(stderr, " -D            Use default values for plot and data files:\n");
    fprintf(stderr, "               'fouran_data.dat' and 'fouran_vals.txt'\n");
    fprintf(stderr, "               Try to run gnuplot program after caclulations to plot a graph.\n");
    fprintf(stderr, "               If successful 'fouran_plot.svg will be created'.\n\n");
    fprintf(stderr, " -f fou_name   Take data from 'for_name' instead of default 'xd_fou.grd'\n");
    fprintf(stderr, "               The file is required for reading cell constants.\n\n");
    fprintf(stderr, " -h            Print this help and exit\n\n");
    fprintf(stderr, " -d dat_name   Write graph data to 'dat_name' file\n\n");
    fprintf(stderr, " -o out_name   Write numeric output to 'out_name' file\n\n");
    fprintf(stderr, " -m mas_name   Read cell data from 'mas_name' file instead of default 'xd.mas'\n\n");
    fprintf(stderr, " -w bin_width  Use bin_width for graph instead of default %f\n\n", DEFAULT_BIN_WIDTH);
    
}

void error_message (const char *format, ...)
{
  va_list a_ptr;
  va_start(a_ptr, format);
  fprintf(stderr, "Error occured: [");
  vfprintf(stderr, format, a_ptr);
  fprintf (stderr, "]\n");
  va_end(a_ptr);
  fprintf (stderr," - Program stopped (try \"%s -h\" to help).\n", P_NAME);
}

void dbg_printf (const char *format, ...)
{
  if (is_debug == 0) return;
  va_list a_ptr;
  va_start(a_ptr, format);
  fprintf(stderr, "DBG: ");
  vfprintf(stderr, format, a_ptr);
  va_end(a_ptr);
}

/* vi:set ts=2: */

int main(int argc, char **argv)
{
    char bfr[MAX_LINE_LENGTH];

    char *fou_name = "xd_fou.grd";
    char *mas_name = "xd.mas";
    char *out_name = NULL;
    char *dat_name = NULL;
    int is_run_gnuplot = 0;

    double bin_width = DEFAULT_BIN_WIDTH;

/* ---- DEAL WITH OPTIONS -------------------------------------------------- */
    while (1) {
        int opt;
        int option_index = 0;
        static struct option long_options[] = {
            {"help", 0, 0, 0},
            {0, 0, 0, 0}
        };

        opt = getopt_long(argc, argv, "Dd:f:hm:o:w:", long_options, &option_index);
        if (opt == -1) break;

        switch (opt) {
            case 'd':
                dat_name = optarg; break;
            case 'f':
                fou_name = optarg; break;
            case 'h': 
                print_help(); exit (EXIT_SUCCESS);
            case 'm':
                mas_name = optarg; break;
            case 'o':
                out_name = optarg; break;
            case 'D':
                is_run_gnuplot = 1;
                dat_name = "fouran_data.dat";
                out_name = "fouran_vals.txt"; break;
            case 'w':
                bin_width = atof(optarg); break;
            default:
//                error_message("");
                exit(EXIT_FAILURE);
        }
    }
    
/* ========================================================================== */
/* ======= READ CELL VOLUME FROM *.mas file ================================= */
/* ========================================================================== */

    double cell_volume = -1;
    
    FILE *inf_h = fopen(mas_name, "r");
    if (!inf_h) {
        error_message("Cannot open file '%s' for reading", mas_name);
        exit(EXIT_FAILURE);
    }

    while (fgets(bfr, MAX_LINE_LENGTH, inf_h)) {
        if (strncmp(bfr, "CELL " , 5) == 0) {
            double a, b, c, A, B, G;
            int r = sscanf(bfr, "%*s%lf%lf%lf%lf%lf%lf", 
                           &a, &b, &c, &A, &B, &G);
            if (r != 6) {
                error_message("Error reading CELL line, file '%s'", mas_name);
                exit(EXIT_FAILURE);
            }
            double degrad=M_PI/180.0;
            double cosA = cos(A*degrad);
            double cosB = cos(B*degrad);
            double cosG = cos(G*degrad);
            double cos2A = cosA*cosA;
            double cos2B = cosB*cosB;
            double cos2G = cosG*cosG;
            cell_volume = a*b*c*
                          sqrt(1 + 2*cosA*cosB*cosG - cos2A - cos2B - cos2G);
            dbg_printf("Cell volume from '%s' file: %12.6f\n", 
                       mas_name, cell_volume);
            break;
        }
    }
    if (cell_volume < 0.0) {
        error_message("Cannot read cell volume from '%s' file", mas_name);
        exit(EXIT_FAILURE);
    }
    
    fclose(inf_h);

/* ========================================================================== */
/* ======= READ DATA FROM XD_FOU.GRD file =================================== */
/* ========================================================================== */

    inf_h = fopen(fou_name, "r");
    if (! inf_h ) {
        error_message("Cannot open file '%s' for reading", fou_name);
        exit(EXIT_FAILURE);
    }

    const char *FLAG_GRID = "! Gridpoints";
    const char *FLAG_VALUES = "! Values";

    unsigned long nx, ny, nz;
    unsigned long n_points_read = 0;

    double ***points = NULL;

    #define READ_NONE 0
    #define READ_GRID_DIMENSIONS 1
    #define READ_VALUES 2
    int read_flag = READ_NONE;

    int ci = 0, cj = 0, ck = 0;
    double rho_min = 1e100, rho_max = -1e100;
    double rho_gross = 0.0, rho_net = 0.0;
    while (fgets(bfr, MAX_LINE_LENGTH, inf_h)) {
        if (strncasecmp(bfr, FLAG_GRID, strlen(FLAG_GRID)) == 0) { 
              read_flag = READ_GRID_DIMENSIONS;
              continue;
        } else if (strncasecmp(bfr, FLAG_VALUES, strlen(FLAG_VALUES)) == 0) { 
              read_flag = READ_VALUES;
              continue;
        } else if (read_flag == READ_GRID_DIMENSIONS) {
            int r = sscanf(bfr, "%lu%lu%lu", &nx, &ny, &nz);
            if (r != 3) {
                error_message("Cannot read grid points from file '%s'\n", fou_name);
                exit(EXIT_FAILURE);
            }
            if (nx < 2 || ny < 2 || nz < 2) {
                error_message("This version works only with 3d data grid"
                " but dimensions are %d %d %d", nx, ny, nz);
                exit(EXIT_FAILURE);
            }
            
            // allocate memory for points array
            points = (double ***)malloc(nx*sizeof(double **));
            if (! points ) { 
                error_message("Memory allocation error");
                exit(EXIT_FAILURE);
            }
            for (int j=0; j<ny; j++) {
                points[j] = (double **)malloc(ny*sizeof(double *));
                if (! points[j] ) { 
                    error_message("Memory allocation error");
                    exit(EXIT_FAILURE);
                }
                for (int k=0; k<nz; k++) {
                    points[j][k] = (double *)malloc(nz*sizeof(double));
                    if (! points[j][k] ) { 
                        error_message("Memory allocation error");
                        exit(EXIT_FAILURE);
                    }
                }
            }
            read_flag = READ_NONE;
            continue;
        } else if (read_flag == 2) {
            unsigned short L = strlen(bfr)-2; 
            for (unsigned short i=0; i<L; i+=DATA_W) {
                double v;
                errno = 0;
                v = strtod(bfr+i, NULL);
                if (errno != 0 ) {
                    error_message("Error converting data!");
                    exit(EXIT_FAILURE);
                }
                points[ci][cj][ck] = v;
                n_points_read++;
                ck++;
                if (ck == nz) {
                    ck = 0;
                    cj++;
                    if (cj == ny) {
                        cj = 0;
                        ci++;
                        if (ci == nx) {
                            break;
                        }
                    }
                }
            }
            
        }
    }
    fclose(inf_h);
    dbg_printf("Reading '%s' complete: %lu x %lu x %lu grid, %lu points\n",
               fou_name, nx, ny, nz, n_points_read);


/* ========================================================================== */
/* ======= PROCESS DATA IN ARRAY  =========================================== */
/* ========================================================================== */

    size_t nbx = nx - 1;
    size_t nby = ny - 1;
    size_t nbz = nz - 1;
    double dr = 1.0;
    double drdr = dr*2;
    size_t nbins = drdr / bin_width;

    size_t *bins = malloc (nbins*sizeof(size_t));
    for (size_t b=0; b<nbins; b++) bins[b] = 0;
    dbg_printf("Memory allocated for %lu bins\n", nbins);

//double d2min = 0.0, d2max = 0.0;

#ifdef OPENMP
#pragma omp parallel 
#endif
{
    size_t *bins_private = malloc (nbins*sizeof(size_t));
    for (size_t b=0; b<nbins; b++) bins_private[b] = 0;
#ifdef OPENMP
#pragma omp for nowait reduction(+:rho_gross, rho_net) reduction(min:rho_min) reduction(max:rho_max)
#endif
    for (int i=0; i<nbx; i++) {
        for (int j=0; j<nby; j++) {
            for (int k=0; k<nbz; k++) {
                double v = points[i][j][k];
                
                if (v < rho_min) rho_min = v;
                else if (v > rho_max) rho_max = v;
                
                rho_gross += fabs(v);
                rho_net += v;
                double vnext[3];
                vnext[0] = points[i][j][k+1];
                vnext[1] = points[i][j+1][k];
                vnext[2] = points[i+1][j][k];
                
                for (int m=0; m<3; m++) {
                    int bin1, bin2;
                    if ( v < vnext[m] ) {
                        bin1 = ceil(v / bin_width);
                        bin2 = ceil(vnext[m] / bin_width);
                    } else {
                        bin1 = ceil(vnext[m] / bin_width);
                        bin2 = ceil(v / bin_width);
                    }

                    // FIXME!!! Bin out of range!!!

                    for (int bi=bin1; bi<bin2; bi++) {
#ifdef OPENMP
                        bins_private[bi + nbins / 2]++; 
#else
                        bins[bi + nbins / 2]++; 
#endif
                    }

                }

            }
        }
    }
#ifdef OPENMP
#pragma omp critical 
    for (size_t b=0; b<nbins; b++)  bins[b] += bins_private[b];
#endif
    dbg_printf("BINNING COMPLETE\n");
}

    unsigned long nxyz = nx*ny*nz;
    unsigned long nbxyz = nbx*nby*nbz;

    rho_gross /= (2*nbxyz);
    rho_net /= (2*nbxyz);

    double e_gross = rho_gross * cell_volume;
    double e_net = rho_net * cell_volume;

    #define SPACE_D 3 
    unsigned long n_d0 = bins[nbins/2];
    double df0 = log(n_d0)/log( pow( (SPACE_D*nbxyz), (1./SPACE_D)) );

    FILE *outf_h = stderr, *dat_h = stdout;
    
    if (out_name != NULL) outf_h = fopen(out_name, "w");
    if (dat_name != NULL) dat_h = fopen(dat_name, "w");

    double rho_delta = rho_max - rho_min;

    fprintf(outf_h, "Grid dimensions:  %8ld %8ld %8ld\n", nx, ny, nz);
    fprintf(outf_h, "nxyz:       %12ld\n", nxyz);
    fprintf(outf_h, "nbxyz:      %12ld\n", nbxyz);
    fprintf(outf_h, "\n");
    fprintf(outf_h, "rho0 min / max / delta: %12.6f%12.6f%12.6f\n", rho_min, rho_max, rho_delta);
    fprintf(outf_h, "\n");
//    fprintf(outf_h, "rho_gross:  %12.6f\n", rho_gross);
//    fprintf(outf_h, "rho_net:    %12.6f\n", rho_net);
    fprintf(outf_h, "e_gross / e_net:        %12.6f%12.6f\n", e_gross, e_net);
    fprintf(outf_h, "\n");
    fprintf(outf_h, "n_d0: %12ld\n  df0: %12.6lf\n", n_d0, df0);
    
    for (unsigned int i=0; i<nbins; i++) {
        unsigned int inbin = bins[i];
        if (inbin == 0) continue;
        
        double rho0 = ((double)i-(double)nbins/2)*bin_width;

        double N = (double)inbin;
        double epsilon = pow( (SPACE_D*nbxyz), (1./SPACE_D) );
        double f = log(N) / log(epsilon);

        fprintf (dat_h, "%12.6f %12.6f %12.6f\n", rho0, f, N/(double)nbxyz);
    }
    
    if (outf_h) {
        fclose(outf_h);
    }
    if (dat_h) {
        fclose(dat_h);
    }


    if (dat_name != NULL) {
        FILE *plt_h;
        plt_h = fopen("fouran_plot.plt", "w");
    
        fprintf(plt_h, "#!/usr/bin/gnuplot\n\
set term svg enh font 'Arial, 9'\n\
set output 'fouran_plot.svg'\n\
set object 1 rect from screen 0, 0, 0 to screen 1, 1, 0 behind\n\
set object 1 rect fc  rgb 'white'  fillstyle solid 1.0\n\
\n\
set xtics 0.1 nomirror\n\
set ytics 0.5 nomirror\n\
set mxtics 5\n\
set grid xtics ytics\n\
\n\
set xlabel 'ρ_0 [e Å^{-3}]'\n\
set ylabel 'd^f'\n\
\n\
plot [-1:1][0.5:3] '%s' ls 7 lc rgb '#0000ee' ps 0.6 notitle\n", dat_name);    
    
    fclose(plt_h);
    } /* if dat_name */
    
    if (is_run_gnuplot != 0) {
        int r;
        r = system("gnuplot < 'fouran_plot.plt'");
//        fprintf(stderr, "r:%d\n", r);
        if (r != 0) {
            fprintf(stderr, "WARNING! gnuplot execution failed\n");
        }
    }

    return 0;
}
