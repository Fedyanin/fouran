#**************************************************************************
#   Copyright (C) 2016 by Ivan Fedyanin                                   *
#   octy@xrlab.ineos.ac.ru                                                *
#                                                                         *
#   This file is a part of fouran program distribution                    *
#                                                                         *
#   fouran is free software: you can redistribute it and/or modify        *
#   it under the terms of the GNU General Public License as published by  *
#   the Free Software Foundation, either version 3 of the License, or     *
#   (at your option) any later version.                                   *
#                                                                         *
#   This program is distributed in the hope that it will be useful,       *
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#   GNU General Public License for more details.                          *
#                                                                         *
#   You should have received a copy of the GNU General Public License     *
#   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
#                                                                         *
#**************************************************************************

CC=gcc
LD=$(CC)
INSTALL=install

PROG_NAME=fouran

CFLAGS=-O3 -Wall -pipe -std=gnu99 -Werror -pedantic -fopenmp  -pthread -DOPENMP 
LDFLAGS=-lm

OBJ = fouran.o
HEADERS=

$(PROG_NAME): $(HEADERS) $(OBJ) 
		$(LD) -s -o  $(PROG_NAME) $(OBJ) $(LDFLAGS)  $(LIBS) -fopenmp

.c.o:
		$(CC) $(CFLAGS) $(CSWITCHES) -c $*.c -o $*.o

clean:
		$(RM) $(OBJ) $(PROG_NAME)

